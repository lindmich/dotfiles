
#!/bin/bash

# set microphone by name
# list with pactl list sources
#mic="alsa_input.usb-Focusrite_Scarlett_Solo_USB_Y7CD46B296067E-00.HiFi__scarlett2i_mono_in_USB_0_0__source"
mic="alsa_input.usb-Focusrite_Scarlett_Solo_USB_Y7CD46B296067E-00.HiFi__Mic1__source"

# set sounds for mute and unmute
sound_mute_on="/home/$USER/Dokumente/audio/mic_muted_ts.wav"
sound_mute_off="/home/$USER/Dokumente/audio/mic_unmuted_ts.wav"
volume="0,6"

# check mic state
output=$(pactl get-source-mute $mic)

# toggle mute state
if [[ $output =~ "ja" || $output =~ "yes" ]]; then
	# Mic is muted. Turn mute off
	pactl set-source-mute $mic 0
	pw-play --volume $volume $sound_mute_off
else
	# Mic is not muted. Turn mute on
	pactl set-source-mute $mic 1
	pw-play --volume $volume $sound_mute_on
fi


